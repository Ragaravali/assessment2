import { NgModule } from '@angular/core';
import{LoginComponent} from './login/login/login.component';
import{MaincomponentComponent} from '../app/main/maincomponent/maincomponent.component';
import{RegisterComponent} from './register/register/register.component';
import {RouterModule, Routes} from '@angular/router';
import{SecureGuard } from '../app/secure.guard';
const routes :Routes=[
  {
    path:'login',
    component:  LoginComponent,
    
    
  },
  {
    path:'main',
    component: MaincomponentComponent,
    
  },
  {
    path:'register',
    component: RegisterComponent
  },
  {
    path:'', 
    component:  MaincomponentComponent
  }

]
@NgModule({
  imports: [
    
    RouterModule.forRoot(routes)
   
  ],
  exports:[
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }

import { Component, OnInit } from '@angular/core';
import{LoginService} from '../login.service';
import{NgForm} from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '../../../../node_modules/@angular/router';
import{SocketService} from '../../socket.service';
import { CookieService } from 'ngx-cookie-service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [ CookieService ]
})
export class LoginComponent implements OnInit {
coins:any;
b:boolean;
// d=[{
//   "numbers":"9876567897",
//   "password":"pass1",
//   "image":"assets/cont1.jpg",
//   "name":"scarlett"

// },
// {
// "numbers":"9234567807",
// "password":"pass2",
// "image":"assets/cont2.jpg",
// "name":"annie"
// },
// {
// "numbers":"9234567807",
// "password":"pass3",
// "image":"assets/cont3.jpg",
// "name":"jay"
// }
// ];
  constructor(private http: HttpClient,private service:LoginService,private router:Router,
    private socket:SocketService,private cookieService:CookieService) { }
  onsubmit(c:NgForm)
  {
    console.log(c.value);
    this.service.getdt().subscribe(res => {
      this.coins = res;
  for(let i=0;i<this.coins.length;i++)
  {
    if(this.coins[i].numbers==c.value.numbers&&this.coins[i].password==c.value.password)
    {
      this.router.navigate(['/main']);
      this.cookieService.set('name',this.coins[i]._id);
     this.b= false;
     sessionStorage.setItem("name",this.coins[i]._id);
     this.socket.user(sessionStorage.getItem("name"));
     this.service.log(this.b);
    }
    else{
      this.b=true;
    }
  }
  });
  }
onadd()
 {
  this.router.navigate(['/register']);
}
  ngOnInit() {
   const now:boolean= this.cookieService.check('name');
   if(now)
   {
     this.router.navigate(['/main']);
   }
   else{
     this.router.navigate(['/login']);
   }
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegisterComponent } from './register/register.component';
import{FormsModule,ReactiveFormsModule} from '@angular/forms';
import{ LoginModule} from '../login/login.module';
@NgModule({
  imports: [
    CommonModule,
    LoginModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [RegisterComponent]
})
export class RegisterModule { }

import { Component, OnInit } from '@angular/core';
import{NgForm} from '@angular/forms';
import{LoginService} from"../../login/login.service";
import{Router} from '../../../../node_modules/@angular/router';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  angForm: FormGroup;
  constructor(private service:LoginService,private router:Router,private fb: FormBuilder) { this.createForm()}
fnname;
fname;
ext;

createForm() {
  this.angForm = this.fb.group({
     name: ['', Validators.required ],
     numbers: ['', Validators.required ],
     password: ['', Validators.required ],
     image: ['', Validators.required ]

  });
}

onChange(event: EventTarget,c:NgForm) {
  let eventObj: MSInputMethodContext = <MSInputMethodContext> event;
  let target: HTMLInputElement = <HTMLInputElement> eventObj.target;
  let files: FileList = target.files;
  this.fname = files[0].name;
  this.ext=this.fname.split('.')[1]
  console.log(this.fname);
  console.log(this.ext);
  this.fnname="assets/"+this.fname;
  
  }
  adding(c:NgForm)
  {

    this.service.add(c.value,this.fnname);
  this.router.navigate(['/main']);
  }
  ngOnInit() {
  }
  
}

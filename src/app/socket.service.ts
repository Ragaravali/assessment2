import { Injectable } from '@angular/core';


import { Observable } from 'rxjs';
import * as io from 'socket.io-client';
// /<reference path="../../typings/globals/socket.io-client/index.d.ts" /> 
// import * as io from '../../node_modules/socket.io-client';
// import {ToasterService} from 'angular2-toaster';
@Injectable({
  providedIn: 'root'
})
export class SocketService {
  
  private socket: SocketIOClient.Socket;
  // private  def:string[]=[];
  i;
  d;
  constructor() {  this.socket = io('http://localhost:4000');
  // this.socket.on('cont', data =>
  // {

  //   this.def.push(data);
  // });
  // console.log(this.def);
  }
  public getMessages = () => {
    return Observable.create((observer) => {
        this.socket.on('cont', (message) => {
            observer.next(message);
        });
    });
}
public getstatus = () => {
  return Observable.create((observer) => {
      this.socket.on('typing', (message) => {
          observer.next(message);
      });
  });
}
public getstatusend = () => {
  return Observable.create((observer) => {
      this.socket.on('typingend', (message) => {
          observer.next(message);
      });
  });
}
 online(online){
// for(var i=0;i<this.d.length;i++)
// {
//   if(this.d[i]==online.id)
//   {
//     return "online";
//   }
// }
 }
  sendMessage(msg: string,i) {
    this.socket.emit('send', { message: msg ,name:i.name});
  }
  user(c)
  {
    this.socket.emit('login',{id:c});
  }   
  disconnect(name)
  {
    this.socket.emit('disconnection',name._id);
    console.log(name._id);
  }
  typingmsg(log)
  {
    this.socket.emit("type",{id:log._id});
  }
  typingmsgend(log)
  {
    this.socket.emit("typeend",{id:log._id});
  }
  // HANDLER
  onNewMessage() {
    return Observable.create(observer => {
      this.socket.on('newMessage', msg => {
        observer.next(msg);
      });
    });
  }
}
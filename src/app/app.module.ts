import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import{FormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import{LoginModule} from './login/login.module';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'; 
import{AppService} from './app.service';
import { RouterModule } from '@angular/router';
import{AppRoutingModule} from '../app/app.routing';
import{MainModule} from './main/main.module';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import{RegisterModule} from './register/register.module';
import{CookieService} from 'ngx-cookie-service';
import { MatCardModule, MatIconModule, MatToolbarModule, MatButtonModule, MatFormFieldModule, MatInputModule } from '@angular/material';
// import{ToasterModule} from 'node_modules/angular2-toaster';
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    LoginModule,
    FormsModule,
    ReactiveFormsModule ,
    HttpClientModule,
    RouterModule,
    AppRoutingModule,
    MainModule,
    AngularFontAwesomeModule,
    RegisterModule,
    MatCardModule,
    MatIconModule,
    MatToolbarModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule

    // ToasterModule
  ],
  providers: [AppService,    CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }

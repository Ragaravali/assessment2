import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaincomponentComponent } from './maincomponent/maincomponent.component';
import{AngularFontAwesomeModule } from 'angular-font-awesome';
import{LoginModule} from '../login/login.module';
import{FormsModule,ReactiveFormsModule} from '@angular/forms';
@NgModule({
  imports: [
    CommonModule,
    AngularFontAwesomeModule,
   LoginModule,
   FormsModule,
   ReactiveFormsModule
  ],
  declarations: [MaincomponentComponent]
})
export class MainModule { }

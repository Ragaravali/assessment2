import { Component, OnInit } from '@angular/core';
import{LoginService} from '../../login/login.service';
import{NgForm} from '@angular/forms';
import * as io from 'socket.io-client';
import{SocketService} from '../../socket.service';
import{Router} from '@angular/router'
import { CookieService } from 'ngx-cookie-service';
@Component({
  selector: 'app-maincomponent',
  templateUrl: './maincomponent.component.html',
  styleUrls: ['./maincomponent.component.css']
})
export class MaincomponentComponent implements OnInit {
contacts;
b=true;
i;
// m=[];
inname="";
ser=true;
img; j=0;
log;inputs=[];
msgs = [];
messageText: string;
messages: Array<any>;
typestat;
socket: SocketIOClient.Socket;
  constructor(private service:LoginService,private chatService: SocketService,
    private router:Router,private cookieService:CookieService) { }
  ngOnInit() {
    const cookieExists: boolean = this.cookieService.check('name');
    if(cookieExists)
    {
      this.router.navigate(['/main']);
    }
    else
    {
      this.router.navigate(['/login']);
    }
    this.service.getdt().subscribe(res => {
      this.contacts = res;
     const d= this.cookieService.get("name");
     for(let x=0;x<this.contacts.length;x++)
     {
       if(d==this.contacts[x]._id)
       {
        this.img=this.contacts[x].image;
        this.log=this.contacts[x];
       }
     }
     for(let x=0;x<this.contacts.length;x++)
{
  if(this.log._id!=this.contacts[x]._id)
  {
    this.inputs[this.j]=this.contacts[x];
    this.j++;
  }
 
}
    
  });
 

  localStorage.setItem("users",JSON.stringify(this.msgs));
  this.chatService.onNewMessage().subscribe(msg => {
    console.log('got a msg: ' + msg);
  });
  this.chatService
      .getMessages()
      .subscribe((message) => {
        this.msgs=message;
      });
      this.chatService
      .getstatus()
      .subscribe((typestatus: string) => {
        this.typestat=typestatus;
      });
      this.chatService
      .getstatusend()
      .subscribe((typestatus: string) => {
        this.typestat="";
      });
   }
sendButtonClick(msgInput) {
  this.chatService.sendMessage(msgInput.value,this.log);
  this.service.addmsg(this.log.name,this.i.name,msgInput.value.message);
  this.sendmsg(msgInput);
  this.chatService.typingmsg("");
}
s=false;
mi=[];
sendmsg(msginput)
{
  this.s=true;
  this.mi.push(msginput.value.message);
}
m;
msg;
type;
status;sentmsgs=[];
usermessages=[null];
view(i)
{
  this.status="offline";
  this.usermessages=[];
this.b=false;
this.i=i;
console.log(this.msgs);
for(let y=0;y<this.msgs.length;y++)
{
      if(i._id==this.msgs[y])
      {
        this.status="online";

      }
      }
  
    if(i._id==this.typestat)
    {
      this.type="typing....";
    }
    else{
      this.type="";    }
console.log(this.status);
this.service.getmsg().subscribe(res => {
  this.msg= res;
});
  for(let j=0;j<this.msg.length;j++)
  {
      if((this.msg[j].to==this.log.name) &&( this.msg[j].from==this.i.name))
      {
       this.usermessages.push(this.msg[j].mesg);
      }
  }
  for(let j=0;j<this.msg.length;j++)
  {
      if((this.msg[j].from==this.log.name) &&( this.msg[j].to==this.i.name))
      {
       this.sentmsgs.push(this.msg[j].mesg);
      }
  }
this.m=this.chatService.online(i);
}
c:Array<string>;
disp()
{
  this.ser=false;
  this.c=this.inputs.filter(item=>item.name.indexOf(this.inname)>-1); 
}
logout()
{
  
 this.chatService.disconnect(this.log);
 this.router.navigate(['/login']);
 this.cookieService.delete('name');
}
typing(e)
{
  if(e==13)
  {
    this.chatService.typingmsgend(this.log);
  }
  else{
    this.chatService.typingmsg(this.log);
  }
}
}